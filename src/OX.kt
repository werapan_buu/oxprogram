val table = arrayOf(
    arrayOf('-','-','-'),
    arrayOf('-','-','-'),
    arrayOf('-','-','-')
)
var row:Int = -1
var col:Int = -1
var player = 'X'
var turn = 0
fun printWelcome(){
    println("Welcome to OX Game")
}
fun printTable() {
    println("  1 2 3")
    var rowIndex = 1
    for(row in table) {
        print("$rowIndex ")
        for(col in row) {
            print("$col ")
        }
        println()
        rowIndex++
    }
}
fun printTurn() {
    println("$player Turn")
}
fun switchPlayer() {
    turn++
    if(player == 'X') {
        player = 'O'
    } else {
        player = 'X'
    }
}
fun input() {
    while(true) {
        try {
            print("Please input row col: ")
            val str = readLine()
            val input = str?.split(" ")
            if(input?.size != 2) {
                println("Error ...")
                continue
            }
            row = input[0].toInt()
            col = input[1].toInt()
            if(row !in 1..3 || col !in 1..3) {
                println("Error ...")
                continue
            }
            if(!setRowCol()) {
                println("Error ...")
                continue
            }
            break

        } catch(t: Throwable) {
            println("Error ...")
        }

    }
}
fun setRowCol(): Boolean {
    if(table[row-1][col-1] != '-') return false
    table[row-1][col-1] = player
    return true
}
fun checkLine(r:Int): Boolean {
    var count = 0
    for(c in 0..2) {
        if(table[r][c]==player) {
            count++
        }
    }

    if(count==3) {
        return true
    }
    return false
}
fun checkCol(c:Int): Boolean {
    var count = 0
    for(r in 0..2) {
        if(table[r][c]==player) {
            count++
        }
    }

    if(count==3) {
        return true
    }
    return false
}
fun checkX1(): Boolean {
    var count = 0
    for(i in 0..2) {
        if(table[i][i]==player) {
            count++
        }
    }
    if(count==3) {
        return true
    }
    return false
}
fun checkX2(): Boolean {
    var count = 0
    for(i in 0..2) {
        if(table[i][2-i]==player) {
            count++
        }
    }
    if(count==3) {
        return true
    }
    return false
}
fun checkWin(): Boolean {
    if(turn == 8) return true
    for(r in 0..2) {
        if(checkLine(r)) {

            return true
        }
    }
    for(c in 0..2) {
        if(checkCol(c)) {

            return true
        }
    }
    if(checkX1()) {

        return true
    }
    if(checkX2()) {

        return true
    }
    return false
}
fun printWin() {
    if(turn==8) {
        println("Draw")
    } else {
        println("$player Win")
    }
}
fun printBye() {
    println("Bye Bye")
}
// Hello
fun main() {
    while(true) {
        printWelcome()
        printTable()
        printTurn()
        input()
        if(checkWin()) {
            break
        }
        switchPlayer()

    }
    printWin()
    printBye()
}